import sys


from iotAmak.env.environment import Environment


class AntEnv(Environment):

    def __init__(self, arguments):
        super().__init__(arguments)


if __name__ == '__main__':
    s = AntEnv(str(sys.argv[1]))
    s.run()