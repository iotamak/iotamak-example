import sys
from ast import literal_eval
from time import sleep

from iotAmak.agent import Agent
from fork import Fork
from random import randrange


class PhiAgent(Agent):

    def __init__(self, identifier: int, broker_ip: str, total_agent):
        self.total_agent = total_agent
        Agent.__init__(self, identifier, broker_ip)

    def on_initialization(self):
        # 0: thinking, 1: hungry, 2: eat
        self.state = 0

        self.subscribe("env/fork/" + str(self.id), self.left_fork_update)
        self.left_fork = Fork(self.id)

        right_id = (self.id - 1) % self.total_agent
        self.subscribe("env/fork/" + str(right_id), self.right_fork_update)
        self.right_fork = Fork(right_id)

        self.fork_wait = True

        self.subscribe("env/agent/" + str(self.id) + "/ask_spoon", self.spoon_response)

    def spoon_response(self, client, userdata, message):
        res = literal_eval(message.payload.decode("utf-8"))
        if res.get("response") == "True":
            if res.get("side") == "right":
                self.right_fork.taken_by = self.id
                self.right_fork.state = res.get("state")
            else:
                self.left_fork.taken_by = self.id
                self.left_fork.state = res.get("state")
        self.fork_wait = False

    def left_fork_update(self, client, userdata, message):
        res = literal_eval(message.payload.decode("utf-8"))
        self.left_fork.state = res.get("state")
        self.left_fork.taken_by = res.get("tanken_by")

    def right_fork_update(self, client, userdata, message):
        res = literal_eval(message.payload.decode("utf-8"))
        self.right_fork.state = res.get("state")
        self.right_fork.taken_by = res.get("tanken_by")

    def ask_spoon(self, left):
        if left :
            self.publish("ask_spoon", "left")
        else :
            self.publish("ask_spoon", "right")
        while self.fork_wait:
            sleep(self.wait_delay)
        self.fork_wait = True

    def on_decide(self):
        super().on_decide()

        if self.right_fork.taken_by == -1:
            self.ask_spoon(False)

        if self.left_fork.taken_by == -1:
            self.ask_spoon(True)

        if self.state == 1:
            if self.right_fork.taken_by != self.id:
                self.ask_spoon(False)
            if self.left_fork.taken_by != self.id:
                self.ask_spoon(True)

    def on_act(self):
        super().on_act()

        if self.state == 0:
            self.log("Is Thinking")
            res = randrange(100)
            if res % 5 == 0:
                self.state = 1
        elif self.state == 1:
            self.log("Is Hungry")
            if self.left_fork.taken_by == self.right_fork.taken_by == self.id:
                self.state = 2
        elif self.state == 2:
            self.log("Is Eating")
            self.publish("done_eating", "")
            self.state = 0


if __name__ == '__main__':
    a = PhiAgent(int(sys.argv[1]), str(sys.argv[2]), int(sys.argv[3]))
    a.run()
