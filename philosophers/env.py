
import sys


from iotAmak.environment import Environment
from fork import Fork


class PhiEnv(Environment):

    def __init__(self, broker_ip, nbr_phil):
        self.nbr_phil = nbr_phil
        super().__init__(broker_ip)

    def on_initialization(self):
        self.forks = []
        for i in range(self.nbr_phil):
            self.forks.append(Fork(i))
            self.subscribe("agent/" + str(i) + "/ask_spoon", self.ask_spoon)
            self.subscribe("agent/" + str(i) + "/done_eating", self.done_eating)

    def ask_spoon(self, client, userdata, message):
        res = str(message.payload.decode("utf-8"))
        agent_id = int(str(message.topic).split("/")[1])

        if res == "left":
            fork_id = agent_id
        else:
            fork_id = (agent_id - 1) % self.nbr_phil

        if self.forks[fork_id].state == 1:
            message = {
                "response": "False"
            }
        elif self.forks[fork_id].taken_by == -1:
            message = {
                "response": "True",
                "side": res,
                "state": 0
            }
            self.forks[fork_id].taken_by = agent_id
        else:
            message = {
                "response": "True",
                "side": res,
                "state": 1
            }
            self.forks[fork_id].taken_by = agent_id
            self.forks[fork_id].state = 1

        self.client.publish("env/agent/" + str(agent_id) + "/ask_spoon", str(message))

    def done_eating(self, client, userdata, message):
        agent_id = int(str(message.topic).split("/")[1])

        for fork_id in [agent_id, (agent_id - 1) % self.nbr_phil]:
            self.forks[fork_id].state = 0

    def on_cycle_begin(self) -> None:
        for fork in self.forks:
            self.client.publish("env/fork/" + str(fork.identifier), str(fork.to_msg()))
            print("Fork : ", fork.identifier," taken by ", fork.taken_by, " and is :", fork.state)


if __name__ == '__main__':
    s = PhiEnv(str(sys.argv[1]), 5)
    s.run()