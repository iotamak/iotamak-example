import sys


from iotAmak.amas import Amas


class PhiAmas(Amas):

    def __init__(self, broker_ip: str, clients, nbr_agent):
        self.agent_to_create = nbr_agent
        super().__init__(broker_ip, clients)

    def on_initial_agents_creation(self):
        for _ in range(self.agent_to_create):
            self.add_agent("philosophers", [str(self.agent_to_create)])


if __name__ == '__main__':
    s = PhiAmas(str(sys.argv[1]), sys.argv[2], 5)
    s.run()
